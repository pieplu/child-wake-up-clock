//@flow
import React from "react";
import { DateTime } from "luxon";
import { useInterval } from "./useInterval/useInterval";
import "./Clock.css";
import { getNowTime } from "./useWakeUp/wakeUpUtils";

type Props = {
  safeNextTime: DateTime,
  safePrevTime: DateTime,
  meridiem?: boolean,
};

export const Clock = ({
  safeNextTime,
  safePrevTime,
  meridiem = false,
}: Props) => {
  const timeFormat = meridiem ? "hh:mm" : "HH:mm";
  const [time, setTime] = React.useState<string>(() =>
    getNowTime().toFormat(timeFormat)
  );

  const [dashOffset, setDashOffset] = React.useState<number>(0);

  useInterval(() => {
    const safeNow = getNowTime();
    const durationFrom: number = safeNow.diff(safePrevTime).as("milliseconds");
    const total: number = safeNextTime.diff(safePrevTime).as("milliseconds");
    const newDashOffset: number = Math.trunc((-183 * durationFrom) / total);

    setDashOffset(newDashOffset);
    setTime(safeNow.toFormat(timeFormat));
  }, 1000);

  return (
    <div className="Clock">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="60"
        height="60"
        viewBox="0 0 60 60"
        className="Clock__icon"
      >
        <circle
          className="Clock__circle"
          cx="30"
          cy="30"
          r="29"
          strokeDashoffset={dashOffset}
        />
      </svg>
      <span className="Clock__digit">{time}</span>
    </div>
  );
};
