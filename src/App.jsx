//@flow
import React from "react";
import "./App.css";
import { useWakeUp } from "./useWakeUp/useWakeUp";
import { Clock } from "./Clock";
import { getConfFromSearch } from "./useWakeUp/getWakeUpConf";
import { getClassNames, toggleFullScreen } from "./utils";
import { Persona } from "./Persona/Persona";
import { modeEnum_ } from "./useWakeUp/wakeUpUtils";
const DEFAULT_CONF = {
  wakeUpConf: { hours: 6, minutes: 0 },
  preWakeUpMinutes: 15,
  sleepConf: { hours: 19, minutes: 0 },
  mode: modeEnum_.combo,
  meridiem: false,
};

function App() {
  const conf = getConfFromSearch(DEFAULT_CONF);
  const { isWakeUp, isPreWakeUp, nextTime, prevTime } = useWakeUp(conf);
  const sleep = !isWakeUp && !isPreWakeUp;
  const modifiers = { sleep, wakeUp: isWakeUp, preWakeUp: isPreWakeUp };
  const displayPersona = [modeEnum_.combo, modeEnum_.minou].includes(conf.mode);
  const displayClock = [modeEnum_.combo, modeEnum_.time].includes(conf.mode);

  return (
    <div className={getClassNames("App", modifiers)}>
      <header className="App-header" onDoubleClick={() => toggleFullScreen()}>
        {displayPersona && <Persona {...modifiers} />}
        {displayClock && (
          <Clock
            safeNextTime={nextTime}
            safePrevTime={prevTime}
            meridiem={conf.meridiem}
          />
        )}
      </header>
    </div>
  );
}

export default App;
