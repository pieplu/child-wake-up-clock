//@flow
import React from "react";
import "./Persona.css";
import { Sleep } from "./Sleep";
import { PreWakeUp } from "./PreWakeUp";
import { WakeUp } from "./WakeUp";
import { getClassNames } from "../utils";

type Props = {
  sleep: boolean,
  preWakeUp: boolean,
  wakeUp: boolean,
};

export const Persona = ({ sleep, preWakeUp, wakeUp }: Props) => {
  return (
    <div
      className={getClassNames("Persona", {
        sleep,
        wakeUp,
        preWakeUp: preWakeUp && !wakeUp,
      })}
    >
      <Sleep />
      <PreWakeUp />
      <WakeUp />
    </div>
  );
};
