import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const timeElement = getByText(/\d{2}:\d{2}/i);

  expect(timeElement).toBeInTheDocument();
});
