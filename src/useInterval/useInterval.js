// @flow
import { type ElementRef } from 'react';
import { useRef, useEffect } from 'react';

export const useInterval = (callback: Function, delay: number | null): void => {
  const savedCallback: ElementRef<any> = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
    return undefined;
  }, [delay]);
};
