//@flow

export const getClassNames = (
  base: string,
  modifiers: { [key: string]: boolean | null | void },
  other?: string
): string => {
  return [
    base,
    ...Object.entries(modifiers).map(([modifier, should]) => {
      return !!should ? `${base}--${modifier}` : "";
    }),
    other,
  ]
    .filter((elem: string | void) => !!elem && !!elem.trim())
    .join(" ");
};

const openFullscreen = (elem: HTMLElement) => {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
    // $FlowFixMe
  } else if (elem.mozRequestFullScreen) {
    // $FlowFixMe
    elem.mozRequestFullScreen();
    // $FlowFixMe
  } else if (elem.webkitRequestFullscreen) {
    // $FlowFixMe
    elem.webkitRequestFullscreen();
    // $FlowFixMe
  } else if (elem.msRequestFullscreen) {
    // $FlowFixMe
    elem.msRequestFullscreen();
  }
};

const exitFullscreen = () => {
  if (document.exitFullscreen) {
    document.exitFullscreen();
    // $FlowFixMe
  } else if (document.mozCancelFullScreen) {
    // $FlowFixMe
    document.mozCancelFullScreen();
    // $FlowFixMe
  } else if (document.webkitExitFullscreen) {
    // $FlowFixMe
    document.webkitExitFullscreen();
    // $FlowFixMe
  } else if (document.msExitFullscreen) {
    // $FlowFixMe
    document.msExitFullscreen();
  }
};
const getFullScreenElement = () => {
  return (
    document.fullscreenElement ||
    //$FlowFixMe
    document.webkitFullscreenElement ||
    //$FlowFixMe
    document.mozFullScreenElement ||
    //$FlowFixMe
    document.msFullscreenElement
  );
};

export function toggleFullScreen() {
  if (!getFullScreenElement() && document.documentElement) {
    openFullscreen(document.documentElement);
  } else {
    if (document.exitFullscreen) {
      exitFullscreen();
    }
  }
}
