// @flow
import React from "react";

import { useInterval } from "../useInterval/useInterval";
import { getWakeUpDateTime } from "./wakeUpUtils";
import { type Config } from "./wakeUpUtils";
import { type ConfiguredDate } from "./wakeUpUtils";

const DEFAULT_DELAY = 5000;
const MAX_DELAY = 3600000; // One hour
const MIN_DELAY = 1000; // One second

export const useWakeUp = ({
  wakeUpConf,
  preWakeUpMinutes = 10,
  sleepConf = { hours: 19 },
}: Config): ConfiguredDate => {
  const [delay, setDelay] = React.useState(DEFAULT_DELAY);
  const [processed, setProcessed] = React.useState<ConfiguredDate>(
    getWakeUpDateTime({
      wakeUpConf,
      preWakeUpMinutes,
      sleepConf,
    })
  );
  const { isWakeUp, isPreWakeUp } = processed;

  const lastIsWakeUp = React.useRef(isWakeUp);
  const lastPreWakeUp = React.useRef(isPreWakeUp);

  useInterval(() => {
    const processedResult = getWakeUpDateTime({
      wakeUpConf,
      preWakeUpMinutes,
      sleepConf,
    });
    const { isWakeUp, isPreWakeUp, durationToNext } = processedResult;

    const nextDelay = Math.max(
      Math.min(Math.trunc(durationToNext.as("milliseconds") / 2), MAX_DELAY),
      MIN_DELAY
    );
    delay !== nextDelay && setDelay(nextDelay);

    if (
      isWakeUp !== lastIsWakeUp.current ||
      isPreWakeUp !== lastPreWakeUp.current
    ) {
      setProcessed(processedResult);
    }
    lastIsWakeUp.current = isWakeUp;
    lastPreWakeUp.current = isPreWakeUp;
  }, delay);

  return processed;
};
