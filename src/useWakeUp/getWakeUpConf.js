//@flow
import { type Config } from "./wakeUpUtils";
import { modeEnum } from "./wakeUpUtils";
import { type ModeEnumType } from "./wakeUpUtils";

const SLEEP_KEYS = ["sleep", "dodo", "coucher", "couché", "couche"];
const WAKEUP_KEYS = ["wake-up", "wakeup", "debout", "réveil", "reveil"];
const PREWAKEUP_KEYS = ["pre", "calme"];
const DISPLAY_MODE_KEYS = ["mode", "type"];
const AM_PM_KEYS = ["meridiem", "ampm", "pm"];

const getObjFromString = (value: string | boolean) => {
  const safeValue = typeof value === "string" ? value : "";
  const [hours, minutes = 0] = safeValue
    .split(/:|h/)
    .map((val) => parseInt(val, 10))
    .filter((nb) => !isNaN(nb));
  return { hours, minutes };
};

export const getConfFromSearch = (defaultConf: Config): Config => {
  const search = window.location.search;
  const allPaire: Array<[string, string | boolean]> = search
    .replace("?", "")
    .split("&")
    .map((keyValue: string) => {
      const paire = keyValue.split("=");
      if (paire.length === 1) {
        return [keyValue, true];
      }
      const [key, value] = paire;
      return [key, value];
    });

  return allPaire.reduce((acc, [currKey, currValue]) => {
    if (SLEEP_KEYS.includes(currKey.toLowerCase())) {
      return { ...acc, sleepConf: getObjFromString(currValue) };
    }
    if (WAKEUP_KEYS.includes(currKey.toLowerCase())) {
      return { ...acc, wakeUpConf: getObjFromString(currValue) };
    }
    if (PREWAKEUP_KEYS.includes(currKey.toLowerCase())) {
      return { ...acc, preWakeUpMinutes: parseInt(currValue, 10) };
    }
    if (
      DISPLAY_MODE_KEYS.includes(currKey.toLowerCase()) &&
      modeEnum.includes(currValue)
    ) {
      const mode: ModeEnumType = (currValue: any);
      return { ...acc, mode };
    }
    if (AM_PM_KEYS.includes(currKey.toLowerCase())) {
      return { ...acc, meridiem: !!currValue };
    }
    return acc;
  }, defaultConf);
};
