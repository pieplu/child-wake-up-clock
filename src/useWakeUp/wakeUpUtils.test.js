//@flow
import { getWakeUpDateTime, getDefaultTime } from "./wakeUpUtils";
import { type Config } from "./wakeUpUtils";

//$FlowFixMe
test("16heures", () => {
  // Given
  const given: Config = { wakeUpConf: { hours: 6 } };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 16 })
  );
  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(true);
  expect(isPreWakeUp).toBe(true);
});

test("5heures", () => {
  // Given
  const given: Config = { wakeUpConf: { hours: 6 } };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 5 })
  );
  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(false);
  expect(isPreWakeUp).toBe(false);
});

test("5heures 51", () => {
  // Given
  const given: Config = { wakeUpConf: { hours: 6 } };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 5, minutes: 51 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(false);
  expect(isPreWakeUp).toBe(true);
});

test("19h 30", () => {
  // Given
  const given: Config = {
    wakeUpConf: { hours: 6, minutes: 0 },
    preWakeUpMinutes: 15,
    sleepConf: { hours: 19, minutes: 0 },
  };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 19, minutes: 30 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(false);
  expect(isPreWakeUp).toBe(false);
});

test("6heures 10", () => {
  // Given
  const given: Config = { wakeUpConf: { hours: 6 } };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 6, minutes: 10 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(true);
  expect(isPreWakeUp).toBe(true);
});

test("sieste 12h10", () => {
  // Given
  const given: Config = {
    wakeUpConf: { hours: 15, minutes: 15 },
    sleepConf: { hours: 13, minutes: 15 },
  };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 12, minutes: 10 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(true);
  expect(isPreWakeUp).toBe(true);
});

test("sieste 14h00", () => {
  // Given
  const given: Config = {
    wakeUpConf: { hours: 15, minutes: 15 },
    sleepConf: { hours: 13, minutes: 15 },
  };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 14 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(false);
  expect(isPreWakeUp).toBe(false);
});

test("sieste 16h00", () => {
  // Given
  const given: Config = {
    wakeUpConf: { hours: 15, minutes: 15 },
    sleepConf: { hours: 13, minutes: 15 },
  };
  // When
  const { isWakeUp, isPreWakeUp } = getWakeUpDateTime(
    given,
    getDefaultTime({ hours: 16 })
  );

  // Then
  //$FlowFixMe
  expect(isWakeUp).toBe(true);
  expect(isPreWakeUp).toBe(true);
});
