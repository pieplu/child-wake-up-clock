// @flow
import { DateTime, Duration } from "luxon";
import { type DateObjectUnits } from "luxon";

export const modeEnum_ = { minou: "minou", time: "time", combo: "combo" };
export type ModeEnumType = "minou" | "time" | "combo";
export const modeEnum: Array<ModeEnumType> = ["minou", "time", "combo"];

export type Config = {|
  wakeUpConf: DateObjectUnits,
  preWakeUpMinutes?: number,
  sleepConf?: DateObjectUnits,
  mode?: ModeEnumType,
  meridiem?: boolean,
|};

const dateObj: DateObjectUnits = { years: 2000, days: 1, months: 1 };
const defaultTime: DateObjectUnits = {
  hours: 0,
  minutes: 0,
  seconds: 0,
  milliseconds: 0,
};

export const getNowTime = () => DateTime.local().set(dateObj);
export const getDefaultTime = (custom: DateObjectUnits = {}) =>
  DateTime.local().set({ ...dateObj, ...defaultTime, ...custom });

export type ConfiguredDate = {|
  now: DateTime,
  wakeUp: DateTime,
  preWakeUp: DateTime,
  sleep: DateTime,
  isWakeUp: boolean,
  isPreWakeUp: boolean,
  nextTime: DateTime,
  prevTime: DateTime,
  durationToNext: Duration,
|};

const isTimePassed = (now: DateTime, timeWake: DateTime, sleep: DateTime) => {
  if (timeWake >= sleep) {
    return now < timeWake ? now < sleep : now >= timeWake;
  } else {
    return now >= timeWake && now < sleep;
  }
};

export const getWakeUpDateTime = (
  { wakeUpConf, preWakeUpMinutes = 10, sleepConf = { hours: 19 } }: Config,
  now: DateTime = getNowTime()
): ConfiguredDate => {
  const wakeUp = getDefaultTime(wakeUpConf);
  const preWakeUp = wakeUp.minus({ minutes: preWakeUpMinutes });
  const sleep = getDefaultTime(sleepConf);
  const isWakeUp = isTimePassed(now, wakeUp, sleep);
  const isPreWakeUp = isTimePassed(now, preWakeUp, sleep);
  const nextTime = isWakeUp ? sleep : isPreWakeUp ? wakeUp : preWakeUp;
  const prevTime = isWakeUp ? wakeUp : isPreWakeUp ? preWakeUp : sleep;
  const safeNextTime = now > nextTime ? nextTime.plus({ days: 1 }) : nextTime;
  const safePrevTime = now < prevTime ? prevTime.minus({ days: 1 }) : prevTime;
  const durationToNext = safeNextTime.diff(now);

  return {
    now,
    wakeUp,
    preWakeUp,
    sleep,
    isWakeUp,
    isPreWakeUp,
    nextTime: safeNextTime,
    prevTime: safePrevTime,
    durationToNext,
  };
};
