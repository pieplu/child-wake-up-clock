// import 'react-app-polyfill/ie11';
// import 'react-app-polyfill/stable';
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

console.info("Icons by Rikki Lorie from the Noun Project");

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
const onSWUpdate = (registration: ServiceWorkerRegistration) => {
  const waitingWorker: ?ServiceWorker = registration.waiting;
  waitingWorker?.postMessage({ type: "SKIP_WAITING" });
  window.location.reload(true);
};

serviceWorker.register({ onUpdate: onSWUpdate });
