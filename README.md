# Child Wake-up clock

A simple child wake-up clock for your browser

## Configuration

With query param on url, to be saved on bookmarks.

```uri
?sleep=19:00&wakeup=06h10&pre=10
```

|Param key| Accepte format value | Descripion |
|:-:|:-:|:-:|
|`sleep` \| `dodo` \| `coucher` \| `couché` \| `couche` | `HH:mm` \| `HHhmm` \| `HH` |Begin of sleep time|
|`wake-up` \| `wakeup` \| `debout` \| `réveil` \| `reveil`| `HH:mm` \| `HHhmm` \| `HH` |Time to wake-up|
|`pre` \| `calme`|`m` (minutes) | Quiet time before wake-up|
|`mode` \| `type`|`minou` \| `time` \| `combo` | Display mode, default is `combo`|
|`meridiem` \| `ampm` \| `pm`| nothing or anything (`&ampm` or `&meridiem=something`) | Display on 12 hours format|

## Capture

![Screen capture of the app](./Capture-1.png?raw=true "Capture")

## Attibutions

Icons by Rikki Lorie from the Noun Project (CC BY)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
